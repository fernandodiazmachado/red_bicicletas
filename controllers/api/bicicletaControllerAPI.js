var Bicicleta = require('../../models/bicicleta');


exports.bicicleta_list = (req, res) => {
    Bicicleta.allBicis(function (err, allBicis) {
        res.status(200).json({ bicicletas: allBicis });
    })
}

exports.bicicleta_create = (req, res) => {
    const bici = new Bicicleta(
        {
            code: req.body.code,
            color: req.body.color,
            modelo: req.body.modelo,
            ubicacion: [req.body.lat, req.body.lng]
        });
    Bicicleta.add(bici, (err, newBici) => {
        if (err){res.status(500)}
        else{
        res.status(200).json({
            bicicleta: newBici
        });
        }
    });
}

//con mismo id en body para el update (siguiendo el criterio del delete)
exports.bicicleta_update = (req, res) => {
    const bici={
        _id: req.body._id,
        code: req.body.code,
        color: req.body.color,
        modelo: req.body.modelo,
        ubicacion: [req.body.lat, req.body.lng]
    };
    Bicicleta.updateOne(bici, (err, updatedBici) => {
        if (err){
            res.status(500)
        }
        else{
        res.status(200).json({
            bicicleta: updatedBici
        });
        }
    });
}

exports.bicicleta_delete = (req, res) => {
    
    Bicicleta.removeByCode(req.body.code,  (err, deletedBici) => {
        if (err){
            res.status(500)
        }
        else{
            res.status(204).send();
        }
    });
    
}

exports.bicicleta_list_old = (req, res) => {
    res.status(200).json({
        bicicletas: Bicicleta.allBicis
    });
}

exports.bicicleta_create_old = (req, res) => {
    var bici = new Bicicleta(req.body.id, req.body.color, req.body.modelo);
    bici.ubicacion = [req.body.lat, req.body.lng];
    Bicicleta.add(bici);
    res.status(200).json({
        bicicleta: bici
    });
}

//con mismo id en body para el update (siguiendo el criterio del delete)
exports.bicicleta_update_old = (req, res) => {
    const bici = Bicicleta.findById(req.body.id);
    bici.color = req.body.color;
    bici.modelo = req.body.modelo;
    bici.ubicacion = [req.body.lat, req.body.lng];
    res.status(200).json({
        bicicleta: bici
    });
}

exports.bicicleta_delete_old = (req, res) => {
    Bicicleta.removeById(req.body.id);
    res.status(204).send();
}