var express = require('express');
var router = express.Router();
var usuarioController = require('../controllers/usuarios');

/* GET users listing. 
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});*/
router.get('/', usuarioController.list);
router.get('/create', usuarioController.create_get);
router.post('/create', usuarioController.create_post); //_post
router.post('/:id/delete', usuarioController.delete); //_post
router.get('/:id/update', usuarioController.update_get);
router.post('/:id/update', usuarioController.update); //_post

module.exports = router;
