# Desarrollo del lado del servidor - NodeJS, Express y MongoDB
## Red Bicicletas

Link al repositorio en bitbucket: https://bitbucket.org/fernandodiazmachado/red_bicicletas/src/master/

Para clonar a tu entorno local: git clone https://bitbucket.org/fernandodiazmachado/red_bicicletas.git

Para instalar las dependencias: npm install

Para levantar el servidor con nodemon: npm run devstart

